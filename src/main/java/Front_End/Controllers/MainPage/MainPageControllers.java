package Front_End.Controllers.MainPage;

import Front_End.Client.Client;
import Front_End.Controllers.Posts.HomeController;
import Front_End.Controllers.ProfilePage.ProfilePageController;
import Front_End.PagesLoader;
import Front_End.Client.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainPageControllers implements Initializable {

    @FXML
    private ImageView profile = new ImageView();
    @FXML
    private ImageView home = new ImageView();
    @FXML
    private ImageView search = new ImageView();
    @FXML
    private ImageView uploadPost = new ImageView();
    @FXML
    private ImageView activity = new ImageView();
    @FXML
    private ScrollPane scrollPane = new ScrollPane();

    @FXML
    public void handleProfileClick() throws IOException{
        makeAllWhite();
        profile.setImage(new Image(new FileInputStream("ProjectExtensions\\64096.png")));
        Client client = Client.getInstance();
        ProfilePageController profilePageController = client.Profile(UserInfo.getInstance().getUsername());

        HomeController homeController = new HomeController( );
        homeController.setAllPostNodes(client.receivePosts(UserInfo.getInstance( ).getUsername( )));
        homeController.addPosts();
        profilePageController.setPostsContent(homeController.getPosts());

        PagesLoader pagesLoader = PagesLoader.getInstance();
        ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene().lookup("#scrollPane");
        scrollPane.setContent(profilePageController.setPage());

    }
    @FXML
    public void handleHomeClick() throws IOException{
        makeAllWhite();
        home.setImage(new Image(new FileInputStream("ProjectExtensions\\20176.png")));
        Client client = Client.getInstance();

        HomeController homeController = new HomeController( );
        homeController.setAllPostNodes(client.receivePosts(UserInfo.getInstance( ).getUsername( )));
        homeController.addPosts();
        homeController.loadPage();

    }
    @FXML
    public void handleSearchClick() throws IOException{
        makeAllWhite();
//        profile.setImage(new Image(new FileInputStream()));
        scrollPane.setContent(FXMLLoader.load(getClass().getResource("/Search.fxml")));
    }
    @FXML
    public void handleUploadPostClick() throws IOException{
        makeAllWhite();
        uploadPost.setImage(new Image(new FileInputStream("ProjectExtensions\\25668.png")));
        scrollPane.setContent(FXMLLoader.load(getClass().getResource("/AddPost.fxml")));
    }
    @FXML
    public void handleActivityClick() throws FileNotFoundException{
        makeAllWhite();
        activity.setImage(new Image(new FileInputStream("ProjectExtensions\\60993.png")));

    }

    private void makeAllWhite () throws FileNotFoundException{
        profile.setImage(new Image(new FileInputStream("ProjectExtensions/PNGIX.com_user-login-png-transparent_6210999.png")));
        home.setImage(new Image(new FileInputStream("ProjectExtensions/home-icon.png")));
        uploadPost.setImage(new Image(new FileInputStream("ProjectExtensions/new-icon.png")));
        activity.setImage(new Image(new FileInputStream("ProjectExtensions/25424.png")));
//        profile.setImage(new Image(new FileInputStream()));
    }


    @Override
    public void initialize(URL location, ResourceBundle resources){

    }
}
