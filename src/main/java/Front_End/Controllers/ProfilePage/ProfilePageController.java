package Front_End.Controllers.ProfilePage;

import Front_End.Client.UserInfo;
import Front_End.Client.Client;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import static javafx.scene.paint.Color.rgb;

public class ProfilePageController implements Initializable {


    private String username ;
    private String userStatus ;
    private String followings;
    private String followersCount;
    private String postsCount;

    public void setUsername(String username){
        this.username = username;
    }

    public void setUserStatus(String userStatus){
        this.userStatus = userStatus;
    }

    public void setFollowings(String followings){
        this.followings = followings;
    }

    public void setFollowersCount(String followersCount){
        this.followersCount = followersCount;
    }

    public void setPostsCount(String postsCount){
        this.postsCount = postsCount;
    }

    private AnchorPane anchorPane = new AnchorPane();
    private ScrollPane postsLayout = new ScrollPane();
    private Circle profileShape = new Circle();
    private Image userProfileImage ;
    private Button button  = new Button();
    private Text usernametxt = new Text();
    private Text postNum  = new Text();
    private Text post = new Text();
    private Text followersNum = new Text();
    private Text followers = new Text();
    private Text followsNum = new Text();
    private Text follows = new Text();


    public void setUsernametxt(){
        usernametxt.setText(username);
        usernametxt.setTranslateY(50);
        usernametxt.setTranslateX(222);
        usernametxt.setFont(Font.font(20));
    }


    public void setPostNum(){
        postNum.setText(postsCount);
        postNum.setTranslateY(82);
        postNum.setTranslateX(222);
    }

    public void setPost(){
        post.setText("Post");
        post.setTranslateY(106);
        post.setTranslateX(211);
    }

    public void setFollowersNum(){
        followersNum.setText(followersCount);
        followersNum.setTranslateX(314);
        followersNum.setTranslateY(82);
    }

    public void setFollowers(){
        followers.setText("Followers");
        followers.setTranslateY(107);
        followers.setTranslateX(317);
    }

    public void setFollowsNum(){
        followsNum.setText(followings);
        followsNum.setTranslateY(82);
        followsNum.setTranslateX(468);
    }

    public void setFollows(){
        follows.setText("Following");
        follows.setTranslateY(106);
        follows.setTranslateX(440);
    }

    public void setUserProfile() throws FileNotFoundException{
        userProfileImage = new Image(new FileInputStream("ProjectExtensions/PNGIX.com_user-login-png-transparent_6210999.png"));
        profileShape.setRadius(40);
        profileShape.setEffect(new DropShadow(8, rgb(0, 0, 0, 0.8)));
        profileShape.setFill(new ImagePattern(userProfileImage));
        profileShape.setTranslateX(50);
        profileShape.setTranslateY(50);
    }

    public void setButton(){

       if ( userStatus.equals("editProfile") ){
           userStatus = "Follow";
       }else  {
           userStatus = "UnFollow";
       }
       if ( username.equals(UserInfo.getInstance().getUsername()) ){
           userStatus = "Edit Profile";
       }
        button.setText(userStatus);
        button.setTranslateY(150);
        button.setTranslateX(20);
        button.setPrefSize(511,36);
    }

    public void handleButton(){
        button.setOnAction(new EventHandler<ActionEvent>( ) {
            @Override
            public void handle(ActionEvent event){
                if ( userStatus.equals("Follow")  ){
                    try {
                        Client.getInstance().Follow(UserInfo.getInstance().getUsername() , username);
                        followersCount = String.valueOf(Integer.parseInt(followersCount)+1);
                        followersNum.setText(followersCount);
                        userStatus = "UnFollow";
                        button.setText("UnFollow");
                    } catch (IOException e) {
                        e.printStackTrace( );
                    }
                }else {
                    if ( userStatus.equals("UnFollow") ) {
                        try {
                            Client.getInstance( ).Follow(UserInfo.getInstance( ).getUsername( ), username);
                            followersCount = String.valueOf(Integer.parseInt(followersCount) - 1);
                            followersNum.setText(followersCount);
                            userStatus = "Follow";
                            button.setText("Follow");
                        } catch (IOException e) {
                            e.printStackTrace( );
                        }
                    }
                }
            }
        });

    }

    public AnchorPane setPage() throws FileNotFoundException{

        anchorPane.setPrefSize(530,630);

        postsLayout.setTranslateX(1);
        postsLayout.setTranslateY(196);
        postsLayout.setPrefSize(550,444);


        setPostNum();
        setPost();
        setFollowersNum ();
        setFollowers();
        setFollowsNum();
        setFollows();
        setUserProfile();
        setButton();
        handleButton();

        setUsernametxt();
        anchorPane.getChildren().addAll(button,post,postNum,postsLayout,followers,followersNum
                ,follows,followsNum,profileShape,usernametxt);

        return anchorPane;
    }

    public void setPostsContent(VBox vBox){
        postsLayout.setContent(vBox);
    }
    @Override
    public void initialize(URL location, ResourceBundle resources){
        handleButton();
    }
}
