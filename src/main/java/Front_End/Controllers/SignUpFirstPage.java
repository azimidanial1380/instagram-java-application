package Front_End.Controllers;

import Front_End.Client.Client;
import Front_End.PagesLoader;
import Front_End.Client.UserInfo;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class SignUpFirstPage {

    private Client client ;
    @FXML
    public TextField name_txt = new TextField();
    @FXML
    private  Button Next_btn = new Button();

    public String getName_txt(){
        return name_txt.getText();
    }

    @FXML
    private void setNext_btn(){
        client = Client.getInstance();
        client.setId(getName_txt());
        UserInfo.getInstance().setUsername(getName_txt());
        PagesLoader pagesLoader = PagesLoader.getInstance();
        pagesLoader.load("/SignUpSecondPage.fxml");
    }

}
