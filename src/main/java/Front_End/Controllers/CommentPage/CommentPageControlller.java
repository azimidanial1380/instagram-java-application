package Front_End.Controllers.CommentPage;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import Front_End.Client.Client;
public class CommentPageControlller implements Initializable {

    private String postID ;

    @FXML
    private Button Send ;
    @FXML
    private ScrollPane scrollPane ;
    @FXML
    private TextField newComment;

    public void handleSearchButton() throws IOException{
       Send.setOnAction(new EventHandler<ActionEvent>( ) {
           @Override
           public void handle(ActionEvent event){
               try {
                   Client.getInstance().Comment(newComment.getText( ),postID );
               } catch (IOException e) {
                   e.printStackTrace( );
               }
           }
       });
//       C
//       CommentPage.userComments.add( new CommentNodes())
    }





    @Override
    public void initialize(URL location, ResourceBundle resources){
        try {
            handleSearchButton();
            postID = CommentPage.PostID ;
            scrollPane.setContent(CommentPage.addNodesToLayout());
        } catch (IOException e) {
            e.printStackTrace( );
        }
    }
}
