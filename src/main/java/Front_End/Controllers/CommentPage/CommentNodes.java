package Front_End.Controllers.CommentPage;

import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static javafx.scene.paint.Color.rgb;

public class CommentNodes {
    private String username ;
    private String comment ;

    public void setComment(String comment){
        this.comment = comment;
    }

    public void setUsername(String username){
        this.username = username;
    }

    private AnchorPane anchorPane = new AnchorPane();
    private Circle profileShape = new Circle();
    private Image userProfileImage ;
    private Text usernametxt = new Text();


    private TextArea commentTxt = new TextArea();

    public void setCommentTxt(){
        commentTxt.setText(comment);
        commentTxt.setTranslateX(96);
        commentTxt.setTranslateY(50);
        commentTxt.setWrapText(true);
        commentTxt.setEditable(false);
        commentTxt.setPrefSize(389,51);
        commentTxt.setScrollTop(0);
    }
    public void setUserProfileImage() throws FileNotFoundException{
        userProfileImage = new Image(new FileInputStream("ProjectExtensions/PNGIX.com_user-login-png-transparent_6210999.png"));
        profileShape.setRadius(20);
        profileShape.setEffect(new DropShadow(8, rgb(0, 0, 0, 0.8)));
        profileShape.setFill(new ImagePattern(userProfileImage));
        profileShape.setTranslateX(42);
        profileShape.setTranslateY(38);
    }

    public void setUsernametxt(){
        usernametxt.setText(username +" :");
        usernametxt.setTranslateY(43);
        usernametxt.setTranslateX(70);
    }

    public AnchorPane getAnchorPane() throws FileNotFoundException{
        anchorPane.setPrefSize(530,100);
        setUsernametxt();
        setUserProfileImage();
        setCommentTxt();
        anchorPane.getChildren().addAll(profileShape,usernametxt , commentTxt);
        return anchorPane ;
    }

}

