package Front_End.Controllers;


import Front_End.Client.Client;
import Front_End.Controllers.Posts.HomeController;
import Front_End.PagesLoader;
import Front_End.Client.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LogInController implements Initializable {

    private Client client ;
    @FXML
    public Button logIn_btn = new Button();
    @FXML
    private TextField userName_txt = new TextField();
    @FXML
    private PasswordField userPassword_pswd = new PasswordField();
    @FXML
    private Button SignUp_btn = new Button();

    public  String getUserName(){
        return userName_txt.getText();
    }

    public  String getUserPassWord(){
        return userPassword_pswd.getText();
    }

    @FXML
    private void handleLogInButtonClick(){
         if (getUserName() != null && getUserPassWord() != null) {
             try {
                 client = Client.getInstance();
                 client.setId(getUserName());
                 client.setPass(getUserPassWord());
                 client.login();
                 if (client.isLoginValid()) {
                     UserInfo userInfo =UserInfo.getInstance();
                     userInfo.setUsername(getUserName());

                     PagesLoader mainPage = PagesLoader.getInstance();
                     mainPage.load("/MainPage.fxml");
                     Client client = Client.getInstance();

                     HomeController homeController = new HomeController( );
                     homeController.setAllPostNodes(client.receivePosts(getUserName()));
                     homeController.addPosts();
                     homeController.loadPage();


                 } else {
                     showDialogError("Password Or Username Is Wrong.","Please Enter Correct Username And Password");
                 }
             } catch (IOException | JSONException e) {
                 e.printStackTrace();
             }

         }else {
             showDialogError("Username Or Password Is Empty","Please Enter Your UserName And Password");
         }


    }

    @FXML
    public void handleSignUpButtonClick(){
         try {
             client = Client.getInstance();
             client.signupButton();
         } catch (IOException e) {
             e.printStackTrace();
         }

         PagesLoader pagesLoader = PagesLoader.getInstance();
         pagesLoader.load("/SignUpFirstPage.fxml");

    }

    private void showDialogError(String headerText , String contentText) {
        Alert nameOrPassError = new Alert(Alert.AlertType.ERROR);
        nameOrPassError.setHeaderText(headerText);
        nameOrPassError.setContentText(contentText);
        nameOrPassError.showAndWait();
    }

    /**

    This function should start connection between server and
    client then server should ask database is username and password
    of this user correct or not!

    then server start connection to client and send that is that true or not.

     */

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }





}
