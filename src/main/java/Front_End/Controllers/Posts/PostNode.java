package Front_End.Controllers.Posts;

import Front_End.Client.Client;
import Front_End.Controllers.CommentPage.CommentNodes;
import Front_End.Controllers.CommentPage.CommentPage;
import Front_End.PagesLoader;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import static javafx.scene.paint.Color.rgb;

public class PostNode  {

    public PostNode(){
        try {
            userProfile = new Image(new FileInputStream("ProjectExtensions\\64096.png"));
        } catch (FileNotFoundException e) {
            e.printStackTrace( );
        }
    }

    private String postID ;
    private ArrayList<PostComment> comments = new ArrayList<>();
    private Image userProfile ;
    private String username ;
    private String postURL ;
    private String likeCounts ;
    private String postCaption ;
    private String postTime ;//will convert to TimeStamp
    private Timestamp postDateTime ;


    private AnchorPane layout = new AnchorPane(  );
    private Circle profileShape = new Circle();
    private Text username_txt = new Text();
    private Text username1_txt=new Text();
    private ImageView postImage=new ImageView();
    private ImageView likeButton = new ImageView();
    private Text postLike= new Text();
    private TextArea postComment= new TextArea();
    private Text showAllComments = new Text("Show all Comments !");
    private Text time = new Text();
    private ImageView setting = new ImageView();
    private ImageView share = new ImageView();
    private ImageView addToFav = new ImageView();
    private ImageView direct = new ImageView();

    public void setExtraImages() throws FileNotFoundException{
        share.setImage(new Image(new FileInputStream("ProjectExtensions/20402.png")));
        setting.setImage(new Image(new FileInputStream("ProjectExtensions/149947.png")));
        addToFav.setImage(new Image(new FileInputStream("ProjectExtensions/25667.png")));
        direct.setImage(new Image(new FileInputStream("ProjectExtensions/54916.png")));
        likeButton.setImage(new Image(new FileInputStream("ProjectExtensions/25424.png")));

        likeButton.setFitHeight(28);
        setting.setFitHeight(28);
        addToFav.setFitHeight(28);
        direct.setFitHeight(28);
        share.setFitHeight(28);

        likeButton.setFitWidth(28);
        share.setFitWidth(28);
        setting.setFitWidth(28);
        addToFav.setFitWidth(28);
        direct.setFitWidth(28);

        likeButton.setTranslateX(15);
        setting.setTranslateX(500);
        addToFav.setTranslateX(500);
        direct.setTranslateX(69);
        share.setTranslateX(121);

        likeButton.setTranslateY(601);
        setting.setTranslateY(11);
        share.setTranslateY(601);
        addToFav.setTranslateY(601);
        direct.setTranslateY(601);
    }

    public void setPostCaption(String postCaption){
        this.postCaption = postCaption;
    }

    public void setLikeCounts(String likeCounts){
        this.likeCounts = likeCounts;
    }

    public void setPostURL(String postURL){
        this.postURL = postURL;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public void setPostID(String postID){
        this.postID = postID;
    }

    public void setPostTime(String postTime){
        this.postTime = postTime;
//        postDateTime = Timestamp.valueOf(postTime);
    }

    public void setComments(ArrayList<PostComment> comments){
        this.comments = comments;
    }

    public void setPostComment(){
        postComment.setText(postCaption);
        postComment.setEditable(false);
    }


    public void setUserProfile() throws FileNotFoundException{
        profileShape.setCenterY(38);
        profileShape.setCenterX(36);
        profileShape.setRadius(20);
        profileShape.setEffect(new DropShadow(8, rgb(0, 0, 0, 0.8)));
        profileShape.setFill(new ImagePattern(userProfile));

    }

    public void setPostImage(){
        try {
            postImage.setFitWidth(530);
            postImage.setFitHeight(530);
            postImage.setImage(new Image(new FileInputStream(postURL)));
        } catch (FileNotFoundException e) {
            e.printStackTrace( );
        }
    }

    public void handleCommentButton(){
        showAllComments.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>( ) {
            @Override
            public void handle(MouseEvent event){
                /**
                  show the comment page
                 */
            }
        });
    }

    public Pane loadPostNode() throws IOException{

        username_txt.setText(username);
        username1_txt.setText(username);
        postLike = new Text(likeCounts);
        setPostComment();
        setPostImage();
        setLikeButton();
        setUserProfile();
        setExtraImages();
        hanldeCommentClick();

        username_txt.setTranslateY(46);
        username_txt.setTranslateX(79);

        handleLikeButton();

        showAllComments.setTranslateX(98);
        showAllComments.setTranslateY(655);

        postLike.setTranslateX(11);
        postLike.setTranslateY(651);
        postLike.setText(likeCounts + "  likes");

        postTime = postTime.substring(postTime.indexOf(":")-2,postTime.lastIndexOf("."));
        time.setText(postTime);
        time.setTranslateY(718);
        time.setTranslateX(11);
        time.setWrappingWidth(70);
        time.setOpacity(1);

        username1_txt.setTranslateY(696);
        username1_txt.setTranslateX(11);

        postComment.setTranslateY(672);
        postComment.setTranslateX(98);
        postComment.setPrefWidth(426);
        postComment.setPrefHeight(37);
        postComment.setStyle("-fx-background-color:transparent;");
        postComment.setWrapText(true);

        postImage.setTranslateY(63);

        layout.getChildren().addAll(profileShape,username1_txt,username_txt,showAllComments,postComment,postImage
        ,likeButton,share,setting,addToFav,direct,time,postLike);

        layout.setPrefSize(500,757);
        layout.setMaxWidth(500);

        return layout;
    }

    public void setLikeButton() throws FileNotFoundException{

        likeButton.setImage(new Image(new FileInputStream("ProjectExtensions/25424.png")));
    }

    public void handleLikeButton (){
        likeButton.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Client c = Client.getInstance();
                try {
                    System.out.println(getUsername()+getPostID() );
                    System.out.println("like Button Clicked" );
                    c.likePost(getUsername(),getPostID());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void hanldeCommentClick(){
        showAllComments.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>( ) {
            @Override
            public void handle(MouseEvent event){
                try {
                ArrayList<CommentNodes> commentNodesArrayList = new ArrayList<>();

                for (int i = 0; i < comments.size( ); i++) {
                    CommentNodes commentNodes = new CommentNodes();
                    commentNodes.setComment(comments.get(i).getText());
                    commentNodes.setUsername(comments.get(i).getUsername());
                    commentNodesArrayList.add(commentNodes);
                    System.out.println(comments.get(i)+"=====================>>>");
                }

                CommentPage.userComments = commentNodesArrayList ;
                CommentPage.PostID = postID ;

                loadPage( );

                } catch (IOException e) {
                    e.printStackTrace( );
                }
            }

            private void loadPage() throws IOException{
                PagesLoader pagesLoader = PagesLoader.getInstance();
                ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene().lookup("#scrollPane");
                scrollPane.setContent(FXMLLoader.load(getClass().getResource("/CommentPage.fxml")));
            }
        });
    }

    public String getPostID(){
        return postID;
    }

    public String getPostTime(){
        return postTime;
    }

    public ArrayList<PostComment> getComments(){
        return comments;
    }

    public String getUsername(){
        return username;
    }

    public Timestamp getPostDateTime(){
        return postDateTime;
    }
}


