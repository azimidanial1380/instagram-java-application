package Front_End.Controllers.Posts;

public class PostComment {


    private String text;
    private String username;

    public PostComment(String text, String username) {
        this.text = text ;
        this.username = username ;
    }

    public String getText() {
        return text;
    }

    public String getUsername() {
        return username;
    }
}
