package Front_End.Controllers.Posts;

import Front_End.PagesLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class HomeController implements Initializable {

    private VBox posts = new VBox();

    List<PostNode> allPostNodes = new ArrayList<>();

    public void setAllPostNodes(ArrayList<PostNode> allPostNodes){
        this.allPostNodes = allPostNodes;
//        allPostNodes.sort(Comparator.comparing(PostNode::getPostDateTime));
        Collections.reverse(allPostNodes);
    }

    public VBox getPosts(){
        return posts;
    }

    public void addPosts() throws IOException{
        for (int i = 0; i < allPostNodes.size( ); i++) {
            try {
                posts.getChildren().add(allPostNodes.get(i).loadPostNode());
            } catch (IOException e) {
                e.printStackTrace( );
            }
        }

    }



    public void loadPage(){
        PagesLoader pagesLoader = PagesLoader.getInstance();
        ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene().lookup("#scrollPane");
        scrollPane.setContent(posts);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources){

    }
}
