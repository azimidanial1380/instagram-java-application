package Front_End.Controllers.Posts;

import Front_End.PagesLoader;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AddPostController implements Initializable {

    @FXML
    private Rectangle Import = new Rectangle();

    private FileChooser fileChooser = new FileChooser();


    public void handleImport() throws IOException{
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("image files","*.png","*.jpg"));
        File selectedFile = fileChooser.showOpenDialog(new Stage());
        if ( selectedFile!=null ){
            String path = selectedFile.getPath();
            path = path.replace("\\","\\\\");
            SubmitPostController.setFilePath(path);

            PagesLoader pagesLoader = PagesLoader.getInstance();
            ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene().lookup("#scrollPane");
            scrollPane.setContent(FXMLLoader.load(getClass().getResource("/SubmitPost.fxml")));

        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources){

    }
}
