package Front_End.Controllers.Posts;

import Front_End.Client.Client;
import Front_End.Client.UserInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


public class SubmitPostController implements Initializable {

    private static String filePath ;

    @FXML
    private Rectangle submitPost = new Rectangle();
    @FXML
    private ImageView postImage = new ImageView();
    @FXML
    private TextArea postCaption = new TextArea();

    public static void setFilePath(String filePath){
        SubmitPostController.filePath = filePath;
    }

    public void setPhoto() throws FileNotFoundException{
        postImage.setFitWidth(221);
        postImage.setFitHeight(214);
        postImage.setImage(new Image(new FileInputStream(filePath)));
    }

    @FXML
    public void handleSubmitPost() throws IOException{

        Client client = Client.getInstance();
        client.addAPost(postCaption.getText(),filePath);

        HomeController homeController = new HomeController( );
        homeController.setAllPostNodes(client.receivePosts(UserInfo.getInstance( ).getUsername( )));
        homeController.addPosts();
        homeController.loadPage();

    }


    @Override
    public void initialize(URL location, ResourceBundle resources){
        try {
            setPhoto();
        } catch (FileNotFoundException e) {
            e.printStackTrace( );
        }
    }
}

