package Front_End.Controllers.SearchPage;

import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class SearchResultPage {

    private static VBox vBox = new VBox();

    public static  ArrayList<UserProfileNodes> allSearchResponse = new ArrayList<>();

    public static VBox loadResposes() throws FileNotFoundException{
        vBox.getChildren().clear();
        for (int i = 0; i < allSearchResponse.size( ); i++) {
            vBox.getChildren().add(allSearchResponse.get(i).getAnchorPane());
        }
        allSearchResponse.removeAll(allSearchResponse);
        return vBox ;
    }

}
