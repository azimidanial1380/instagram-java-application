package Front_End.Controllers.SearchPage;

import Front_End.Client.Client;
import Front_End.Client.UserInfo;
import Front_End.Controllers.Posts.HomeController;
import Front_End.Controllers.ProfilePage.ProfilePageController;
import Front_End.PagesLoader;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static javafx.scene.paint.Color.rgb;

public class UserProfileNodes {
    private String username ;

    public void setUsername(String username){
        this.username = username;
    }

    private AnchorPane anchorPane = new AnchorPane();
    private Circle profileShape = new Circle();
    private Image userProfileImage ;
    private Text usernametxt = new Text();


    public void setUserProfileImage() throws FileNotFoundException{
        userProfileImage = new Image(new FileInputStream("ProjectExtensions/PNGIX.com_user-login-png-transparent_6210999.png"));
        profileShape.setRadius(20);
        profileShape.setEffect(new DropShadow(8, rgb(0, 0, 0, 0.8)));
        profileShape.setFill(new ImagePattern(userProfileImage));
        profileShape.setTranslateX(42);
        profileShape.setTranslateY(38);
    }

    public void setUsernametxt(){
        usernametxt.setText(username);
        usernametxt.setTranslateY(43);
        usernametxt.setTranslateX(96);
    }
    public void handleTextClickes(){
        usernametxt.setOnMouseEntered(new EventHandler<MouseEvent>( ) {
            @Override
            public void handle(MouseEvent event){
                PagesLoader.getInstance().getScene().setCursor(Cursor.HAND);
            }
        });
        usernametxt.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>( ) {
            @Override
            public void handle(MouseEvent event){

                try {
                    loadProfilePage( );
                } catch (IOException e) {
                    e.printStackTrace( );
                }


            }
        });
        PagesLoader.getInstance().getScene().setCursor(Cursor.DEFAULT);
    }

    private void loadProfilePage() throws IOException{
        Client client = Client.getInstance();

        ProfilePageController profilePageController = null;
        profilePageController = client.Profile(username);

        HomeController homeController = new HomeController( );
        homeController.setAllPostNodes(client.receivePosts(username));
        homeController.addPosts();

        profilePageController.setPostsContent(homeController.getPosts());

        PagesLoader pagesLoader = PagesLoader.getInstance();
        ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene().lookup("#scrollPane");
        scrollPane.setContent(profilePageController.setPage());
    }

    public AnchorPane getAnchorPane() throws FileNotFoundException{
        anchorPane.setPrefSize(540,75);
        setUsernametxt();
        setUserProfileImage();
        handleTextClickes();
        anchorPane.getChildren().addAll(profileShape,usernametxt);
        return anchorPane ;
    }

}
