package Front_End.Controllers.SearchPage;

import Front_End.Client.Client;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SearchPageController implements Initializable {

    @FXML
    private Button search ;
    @FXML
    private ScrollPane scrollPane ;
    @FXML
    private TextField searchField;

    public void handleSearchButton(){
        search.setOnAction(new EventHandler<ActionEvent>( ) {
            @Override
            public void handle(ActionEvent event){
                Client c = Client.getInstance();
                try {
                    c.search(searchField.getText());
                } catch (IOException e) {
                    e.printStackTrace( );
                }

            }
        });
    }





    @Override
    public void initialize(URL location, ResourceBundle resources){
        try {
            scrollPane.setContent(SearchResultPage.loadResposes());
        } catch (FileNotFoundException e) {
            e.printStackTrace( );
        }
        handleSearchButton();
    }
}
