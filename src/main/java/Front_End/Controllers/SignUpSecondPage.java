package Front_End.Controllers;

import Front_End.Client.Client;
import Front_End.PagesLoader;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import org.json.JSONException;

import java.io.IOException;

public class SignUpSecondPage {

    private Client client ;
    public static String userName ;

    @FXML
    private PasswordField passwordField = new PasswordField();
    @FXML
    private Button SignUp_btn = new Button();


    public String getPasswordField(){
        return passwordField.getText();
    }

    @FXML
    private void setSingUp_btn() throws JSONException, IOException{
        client = Client.getInstance( );
        client.setPass(getPasswordField());
        client.signup();
        if (client.isSignupValid( )) {
            showDialogError("Error", "choose another username!");
            PagesLoader pagesLoader = PagesLoader.getInstance();
            pagesLoader.load("/SignUpFirstPage.fxml");
        } else {
            PagesLoader pagesLoader = PagesLoader.getInstance();
            pagesLoader.load("/MainPage.fxml");
        }

    }
    private void showDialogError(String headerText , String contentText) {
        Alert nameOrPassError = new Alert(Alert.AlertType.ERROR);
        nameOrPassError.setHeaderText(headerText);
        nameOrPassError.setContentText(contentText);
        nameOrPassError.showAndWait();
    }

}
