package Front_End;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws IOException{

        PagesLoader startPage = PagesLoader.getInstance();
        startPage.setPrimaryStages(primaryStage);
        startPage.load("/Login.fxml");
    }


    public static void main(String[] args) {
        launch(args);
    }
}

