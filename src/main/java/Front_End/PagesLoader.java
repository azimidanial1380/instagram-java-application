package Front_End;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class PagesLoader {

    private static PagesLoader instance = null;
    public Stage primaryStages ;
    private Scene scene = null;


    private PagesLoader(){}

    public void setPrimaryStages(Stage primaryStages){
       this.primaryStages= primaryStages;
    }

    public static PagesLoader getInstance(){
        if (instance==null){
            instance = new PagesLoader();
        }
        return instance;
    }

    public void load(String url){
        Parent root;
        URL resource;
        try {
            resource = getClass().getResource(url);
            root = FXMLLoader.load(resource);
            scene = new Scene(root);
            primaryStages.setTitle("Instagram");
            primaryStages.setScene(scene);
            primaryStages.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Scene getScene(){
        return scene;
    }

    public void closeCurrentStage(){
        primaryStages.close();
    }


}
