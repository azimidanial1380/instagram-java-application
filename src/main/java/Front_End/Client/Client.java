package Front_End.Client;

import Front_End.Controllers.Posts.PostComment;
import Front_End.Controllers.Posts.PostNode;
import Front_End.Controllers.ProfilePage.ProfilePageController;
import Front_End.Controllers.SearchPage.SearchResultPage;
import Front_End.Controllers.SearchPage.UserProfileNodes;
import Front_End.PagesLoader;
import Transmision.Client_Server.JSON.Client_Queries.*;
import Transmision.Client_Server.JSON.Server_Queries.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import org.json.JSONException;

import java.io.*;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;


public class Client {

    private static Client instance = null;
    private final Socket s;
    private final DataInputStream dis;
    private final DataOutputStream dos;
    private final BufferedReader br;
    private Login_Response_Query LoginResponse;
    private Signup_Response_Query signupResponse;
    private String id;
    private String pass;


    public void setId(String id){
        this.id = id;
    }

    public void setPass(String pass){
        this.pass = pass;
    }


    public static Client getInstance(){
        try {
            if ( instance == null ) {
                instance = new Client( );
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace( );
        }
        return instance;
    }

    public Client() throws IOException, JSONException{
        s = new Socket("127.0.0.1", 22222);
        dis = new DataInputStream(s.getInputStream( ));
        dos = new DataOutputStream(s.getOutputStream( ));
        br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("please enter an ID");
        System.out.println("please enter a Password:");
    }

    public void login() throws JSONException, IOException{
        if ( isSignupButtonUsed ) {
            signupButton( );
        } else {
            dos.writeUTF("login");
            Login_Query login = new Login_Query( );

            Login_Query.setLoginJSONObject(id, pass);

            System.out.println(login + "**********");

            dos.writeUTF(login.toString( ));
            System.out.println(dis.readUTF( ));

            LoginResponse = new Login_Response_Query(dis.readUTF( ));

            System.out.println("-------- server to client response: ");
            System.out.println(LoginResponse);
            System.out.print("request type:  ");
            System.out.println(Login_Response_Query.generalKeyToValueMap("request"));
            System.out.print("data (JSON String) : ");
            System.out.println(Login_Response_Query.generalKeyToValueMap("data"));

            System.out.print("login response:  ");
            System.out.println(Login_Response_Query.dataKeyToValueMap("message"));
            isLoginValid( );
        }
    }

    public Boolean isLoginValid() throws JSONException{
        return Login_Response_Query.dataKeyToValueMap("message").equals("ValidateLogin");
    }

    public boolean isSignupButtonUsed = false;

    public void signupButton() throws IOException{
        dos.writeUTF("signup");
        isSignupButtonUsed = true;
    }

    public void signup() throws JSONException, IOException{

        Signup_Query signup = new Signup_Query( );
        Signup_Query.setSignupJSONObject(id, pass);
        dos.writeUTF(signup.toString( ));

        signupResponse = new Signup_Response_Query(dis.readUTF( ));

        System.out.println("-------- server to client response: ");
        System.out.println(signupResponse);
        System.out.print("request type:  ");
        System.out.println(Signup_Response_Query.generalKeyToValueMap("request"));
        System.out.print("data (JSON String) : ");
        System.out.println(Signup_Response_Query.generalKeyToValueMap("data"));
        System.out.print("signup response:  ");
        System.out.println(Signup_Response_Query.dataKeyToValueMap("message"));
        isSignupValid( );
    }

    public Boolean isSignupValid() throws JSONException{
        return Signup_Response_Query.dataKeyToValueMap("message").equals("InvalidateSignup");
    }

    public void addAPost(String caption, String dir) throws IOException{

        dos.writeUTF("addPost");
        String time = LocalDateTime.now( ).toString( );
        String fileFormat = getFormat(dir);

        Post_Query post = new Post_Query( );
        post.setPostJSONObject(caption, time, fileFormat);
        dos.writeUTF(post.toString( ));
        dos.flush( );

        FileClient.sendFile(dos, dir);

        System.out.println(dis.readUTF( ));
        System.out.println(dis.readUTF( ));

    }

    private String getFormat(String dir){
        String[] fileFormat = dir.split("\\.");
        return fileFormat[1];
    }

    public ArrayList<PostNode> receivePosts(String username) throws IOException{

        dos.flush( );
        dos.writeUTF("getPost");
        dos.writeUTF(username);

        ArrayList<PostNode> allPosts = new ArrayList<>( );
        int followerCount = dis.read();

        for (int i = 0; i < followerCount; i++) {
            int postsNumber = dis.read();
            System.out.println(postsNumber );
            for (int j = 0; j < postsNumber; j++) {

                allPosts.add(receivePost( ));
            }
        }
        return allPosts;
    }

    public PostNode receivePost() throws IOException{

        Post_List receivePost = new Post_List(dis.readUTF());



        String postID = receivePost.dataKeyToValueMap("postId");
        String fileFormat = receivePost.dataKeyToValueMap("fileFormat");

        PostNode newPost = new PostNode( );
        newPost.setPostID(postID);
        newPost.setPostCaption(receivePost.dataKeyToValueMap("caption"));
        newPost.setPostTime(receivePost.dataKeyToValueMap("time"));
        newPost.setComments(null);
        newPost.setUsername(receivePost.dataKeyToValueMap("username"));
        newPost.setLikeCounts(receivePost.dataKeyToValueMap("likes"));
        newPost.setComments(receiveComments());

        String filePath = ("clientTempFiles" + "\\\\" + postID + "." + fileFormat);
        FileClient.receiveFile(dos, dis, filePath);
        newPost.setPostURL(filePath);
        dos.writeUTF("Post is received");
        return newPost;
    }

    public void likePost(String username, String postID) throws IOException{
        dos.writeUTF("like");
        Like_Query like = new Like_Query( );
        like.setLikeJSONObject(username, postID);

        dos.writeUTF(like.toString( ));
        dos.flush( );
        System.out.println(dis.readUTF( ));

    }

    public void search(String searchText) throws IOException{
        dos.writeUTF("search");
        Search_Query search = new Search_Query( );
        search.setSearchJSONObject(searchText);

        dos.writeUTF(search.toString( ));
        dos.flush( );
        searchResponse( );

    }

    public void searchResponse() throws IOException{

        ArrayList<UserProfileNodes> userProfileNodesArrayList = new ArrayList<>( );
        int searchResultCount = Integer.parseInt(dis.readUTF( ));

        for (int i = 0; i < searchResultCount; i++) {
            Search_Response_Query searchResult = new Search_Response_Query(dis.readUTF( ));
            UserProfileNodes userProfileNodes = new UserProfileNodes( );
            userProfileNodes.setUsername(searchResult.dataKeyToValueMap("foundUser"));
            userProfileNodesArrayList.add(userProfileNodes);
        }

        SearchResultPage.allSearchResponse = userProfileNodesArrayList;
        PagesLoader pagesLoader = PagesLoader.getInstance( );
        ScrollPane scrollPane = (ScrollPane) pagesLoader.getScene( ).lookup("#scrollPane");

        scrollPane.setContent(FXMLLoader.load(getClass( ).getResource("/Search.fxml")));
    }

    public ProfilePageController Profile(String username) throws IOException{
        dos.writeUTF("Profile");

        Open_Profile_Query Profile = new Open_Profile_Query( );
        Profile.setOpenProfileJSONObject(username);
        dos.writeUTF(Profile.toString( ));

        return receiveProfile();

    }

    public ProfilePageController receiveProfile() throws IOException {
        Profile_Detail_Query profileDetail = new Profile_Detail_Query(dis.readUTF()) ;

        ProfilePageController profilePageController = new ProfilePageController();
        profilePageController.setUsername(profileDetail.dataKeyToValueMap("username"));
        profilePageController.setUserStatus(profileDetail.dataKeyToValueMap("userType"));
        profilePageController.setPostsCount(profileDetail.dataKeyToValueMap("postNumber"));
        profilePageController.setFollowersCount(profileDetail.dataKeyToValueMap("followerNumber"));
        profilePageController.setFollowings(profileDetail.dataKeyToValueMap("followingNumber"));
        System.out.println(profileDetail.dataKeyToValueMap("username")  + "     vsfsvfVsfl");
        dos.flush( );
        System.out.println(dis.readUTF());

        return profilePageController;

    }

    public void Follow(String followerUsername, String toBeFollowedUsername) throws IOException{
        dos.writeUTF("follow");
        Follow_Query follow = new Follow_Query( );
        follow.setFollowJSONObject(followerUsername, toBeFollowedUsername);

        dos.writeUTF(follow.toString( ));
        dos.flush( );
        System.out.println(dis.readUTF( ));
    }


    public ArrayList<PostComment> receiveComments() throws IOException {
        dos.flush();
//        dos.writeUTF("getComment");
//        dos.writeUTF(postID);
        ArrayList<PostComment> comments = new ArrayList<>() ;

        int commentsNumber = Integer.parseInt(dis.readUTF());
        for (int i = 0; i < commentsNumber; i++) {
            comments.add(receiveComment()) ;
        }
//        newPost.setComments(comments);
        return comments;
    }
    public PostComment receiveComment() throws IOException {

        Comment_List_Query receiveComment = new Comment_List_Query(dis.readUTF( ));

        String CommentText = receiveComment.dataKeyToValueMap("text");
        String username = receiveComment.dataKeyToValueMap("username");

        PostComment comment = new PostComment(CommentText ,username);

        return comment;
    }




    public void Comment(String commentText,String postId) throws IOException {
        dos.writeUTF("Comment");
        String time = LocalDateTime.now().toString() ;

        Comment_Query comment = new Comment_Query() ;
        comment.setCommentJSONObject(commentText,time,id,postId);

        dos.writeUTF(comment.toString());
        System.out.println(dis.readUTF());
        dos.flush();
    }

}

