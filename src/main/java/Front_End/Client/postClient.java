package Front_End.Client;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class postClient {
    private final String address;
    private final int port;
    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;

    public postClient(String address, int port) throws IOException, UnknownHostException {
        this.address = address;
        this.port = port;
        // trying to connect
        socket = new Socket(address, port);
        dis = new DataInputStream(socket.getInputStream());
        dos = new DataOutputStream(socket.getOutputStream());

    }

    /** needs a string as the complete address of file*/
    public void sendFile (String dir) throws IOException {
        int transferSize = 1048576;
        File file = new File(dir);
        Path path = Paths.get(dir);
//        dos.writeLong(Files.size(path));
//        dos.flush();
        FileInputStream fis = new FileInputStream(file);

        byte[] buffer = new byte[transferSize];

        int len = fis.available();
        while (len > 0) {
            len = fis.read(buffer);
            if (len < transferSize) {
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                dos.write(tmp);
                dos.flush();
                break;
            }
            dos.write(buffer);
            dos.flush();
            if (fis.available() == 0) {
                break;
            }
        }
        fis.close();
    }

    public void receiveFile (String fileName, long fileSize) throws IOException {
        long readData = 0;
        String folder = "data";
        int transferSize = 1048576;

        Path path = Paths.get(folder);

        byte[] buffer = new byte[transferSize];
        File file = new File(folder + "\\" + fileName);
        FileOutputStream fos = new FileOutputStream(file, true);

        int len;
        while (true) {
            len = dis.read(buffer);
            readData = readData + (long) len;

            byte[] tmp = new byte[len];
            System.arraycopy(buffer, 0, tmp, 0, len);
            fos.write(tmp);
            fos.flush();

            if (readData == fileSize) {
                break;
            }
        }
        fos.close();
    }
}
