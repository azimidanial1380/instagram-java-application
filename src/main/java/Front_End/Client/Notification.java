package Front_End.Client;

import java.awt.*;

public class Notification {
    private String username ;
    public Notification(String username) {
       this.username = username ;
    }

    public void displayTray() throws AWTException {
        SystemTray tray = SystemTray.getSystemTray();

        Image image = Toolkit.getDefaultToolkit().createImage("icon.png");

        TrayIcon trayIcon = new TrayIcon(image, "Tray");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("System tray icon");
        tray.add(trayIcon);

        trayIcon.displayMessage("Welcome " + username + "!" , "notification", TrayIcon.MessageType.INFO);
    }
}
