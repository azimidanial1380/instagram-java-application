package Front_End.Client;

public class UserInfo {

    private static UserInfo instance ;
    private static String username ;

    public static UserInfo getInstance(){
            if (instance==null){
                instance = new UserInfo();
            }
            return instance;
    }

    public  String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
    }

}
