package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Signup_Response_Query {

    public static JSONObject signupResponceJSON ;

    public Signup_Response_Query() {}
    public Signup_Response_Query(String JSONAlike) throws JSONException {
        this.signupResponceJSON = new JSONObject(JSONAlike);
    }

    public void setResponseJSONObject(String Message) throws JSONException {
        String message = "{\"request\":\"signupResponse\"," +
                "\"data\":{" +
                "\"message\":\"" + Message + "\"," +
                "}" +
                "}";
        signupResponceJSON = new JSONObject(message);
    }

    public static JSONObject getResponseJSONObject() {
        return signupResponceJSON;
    }

    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = signupResponceJSON.get(key).toString();
        return value;
    }

    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(signupResponceJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return signupResponceJSON.toString() ;
    }

}
