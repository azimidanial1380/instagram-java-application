package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Login_Response_Query {

    public static JSONObject loginResponceJSON ;

    public Login_Response_Query() {}
    public Login_Response_Query(String JSONAlike) throws JSONException {
        this.loginResponceJSON = new JSONObject(JSONAlike);
    }

    public String setResponseJSONObject(String Message) throws JSONException {
        String message = "{\"request\":\"loginResponse\"," +
                "\"data\":{" +
                "\"message\":\"" + Message + "\"," +
                "}" +
                "}";
        loginResponceJSON = new JSONObject(message);

        return message;
    }

    public static JSONObject getResponseJSONObject() {
        return loginResponceJSON;
    }

    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = loginResponceJSON.get(key).toString();
        return value;
    }

    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(loginResponceJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return loginResponceJSON.toString() ;
    }

}
