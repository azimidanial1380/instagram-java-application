package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Post_List {
    public static JSONObject postListJSON ;


    public Post_List() {}

    public Post_List(String JSONAlike) throws JSONException {
        this.postListJSON = new JSONObject(JSONAlike);
    }


    public static void setPostListJSONObject(String caption , String time, String likes, String username, String postId, String fileFormat) throws JSONException {
        String postList = "{\"request\":\"postList\"," +
                "\"data\":{" +
                "\"caption\":\"" + caption + "\"," +
                "\"time\":\"" + time + "\"," +
                "\"likes\":\"" + likes + "\"," +
                "\"username\":\"" + username + "\"," +
                "\"postId\":\"" + postId + "\"," +
                "\"fileFormat\":\"" + fileFormat + "\"" +
                       "}" +
                "}";
        postListJSON = new JSONObject(postList);
    }

    public static JSONObject getPostListJSONObject() {
        return postListJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = postListJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(postListJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return postListJSON.toString() ;
    }
}
