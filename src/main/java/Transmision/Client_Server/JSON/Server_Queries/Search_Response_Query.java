package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Search_Response_Query {
    public static JSONObject searchResponseJSON ;


    public Search_Response_Query() {}

    public Search_Response_Query(String JSONAlike) throws JSONException {
        this.searchResponseJSON = new JSONObject(JSONAlike);
    }


    public static void setSearchResponseJSONObject(String foundUser) throws JSONException {
        String searchResponse = "{\"request\":\"searchResponse\"," +
                "\"data\":{" +
                    "\"foundUser\":\"" + foundUser + "\"" +
                    "}" +
                "}";
        searchResponseJSON = new JSONObject(searchResponse);
    }

    public static JSONObject getSearchResponseJSONObject() {
        return searchResponseJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = searchResponseJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(searchResponseJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return searchResponseJSON.toString() ;
    }
}
