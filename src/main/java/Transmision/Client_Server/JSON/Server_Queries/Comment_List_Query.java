package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Comment_List_Query {
    public static JSONObject commentListJSON ;


    public Comment_List_Query() {}

    public Comment_List_Query(String JSONAlike) throws JSONException {
        this.commentListJSON = new JSONObject(JSONAlike);
    }


    public static void setCommentListJSONObject(String text , String username ) throws JSONException {
        String commentList = "{\"request\":\"commentList\"," +
                "\"data\":{" +
                    "\"text\":\"" + text + "\"," +
                    "\"username\":\"" + username + "\"" +
                          "}" +
                "}";
        commentListJSON = new JSONObject(commentList);
    }

    public static JSONObject getCommentListJSONObject() {
        return commentListJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = commentListJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(commentListJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return commentListJSON.toString() ;
    }
}
