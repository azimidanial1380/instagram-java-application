package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Post_Response_Query {

        public static JSONObject postResponceJSON ;

        public Post_Response_Query() {}
        public Post_Response_Query(String JSONAlike) throws JSONException {
            this.postResponceJSON = new JSONObject(JSONAlike);
        }

        public String setResponseJSONObject(String Message) throws JSONException {
            String message = "{\"request\":\"postResponse\"," +
                    "\"data\":{" +
                    "\"message\":\"" + Message + "\"," +
                    "}" +
                    "}";
            postResponceJSON = new JSONObject(message);

            return message;
        }
        public static JSONObject getResponseJSONObject() {
            return postResponceJSON;
        }

        public static String generalKeyToValueMap(String key) throws JSONException {
            String value = postResponceJSON.get(key).toString();
            return value;
        }

        // for inside Object of the key: "data" to get values of inside JSON object
        public static String dataKeyToValueMap(String key) throws JSONException {
            JSONObject dataObject = new JSONObject(postResponceJSON.get("data").toString());
            String value = dataObject.get(key).toString();
            return value;
        }

        @Override
        public String toString() {
            return postResponceJSON.toString() ;
        }


}
