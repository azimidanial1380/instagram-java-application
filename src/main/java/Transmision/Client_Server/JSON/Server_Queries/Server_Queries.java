package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Server_Queries {
    
    private static JSONObject JSONResObj ;
    private String jsonResRequest ;
    private String jsonResData ;

    public Server_Queries() {}
    public Server_Queries(String JSONAlike) throws JSONException {
        this.JSONResObj = new JSONObject(JSONAlike);
    }
    public String setResponseJSONObject() throws JSONException {
        String message = "{\"request\":\"" + jsonResRequest +"\"," +
                "\"data\":{" +  jsonResData + "}" + "}";
        JSONResObj = new JSONObject(message);
        return message;
    }
   


    public void loginResponseJSON(String Message){
        this.jsonResRequest = "loginResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void signupResponseJSON(String Message){
        this.jsonResRequest = "signupResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void logoutResponseJSON(String Message){
        this.jsonResRequest = "logoutResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void postResponseJSON(String Message){
        this.jsonResRequest = "postResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void likeResponseJSON(String Message){
        this.jsonResRequest = "likeResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void commentResponseJSON(String Message){
        this.jsonResRequest = "commentResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void followResponseJSON(String Message){
        this.jsonResRequest = "followResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void unfollowResponseJSON(String Message){
        this.jsonResRequest = "unfollowResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }
    public void searchResponseJSON(String Message){
        this.jsonResRequest = "searchResponse" ;
        this.jsonResData = "\"message\":\"" + Message + "\""  ;
    }

    //*******************************************************************************

    public static JSONObject getResponseJSONObject() {
        return JSONResObj;
    }

    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = JSONResObj.get(key).toString();
        return value;
    }

    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(JSONResObj.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return jsonResRequest.toString() ;
    }

}
