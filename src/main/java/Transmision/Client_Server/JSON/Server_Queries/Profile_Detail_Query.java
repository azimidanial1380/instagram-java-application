package Transmision.Client_Server.JSON.Server_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Profile_Detail_Query {
    public static JSONObject profileDetailJSON ;


    public Profile_Detail_Query() {}

    public Profile_Detail_Query(String JSONAlike) throws JSONException {
        this.profileDetailJSON = new JSONObject(JSONAlike);
    }


    public static void setProfileDetailJSONObject(String username ,String userType, String postNumber, String followerNumber, String followingNumber) throws JSONException {
        String profileDetail = "{\"request\":\"profileDetail\"," +
                "\"data\":{" +
                    "\"username\":\"" + username + "\"," +
                    "\"userType\":\"" + userType + "\"," +
                    "\"postNumber\":\"" + postNumber + "\"," +
                    "\"followerNumber\":\"" + followerNumber + "\"," +
                    "\"followingNumber\":\"" + followingNumber + "\"" +
                "}" +
                "}";
        profileDetailJSON = new JSONObject(profileDetail);
    }

    public static JSONObject getProfileDetailJSONObject() {
        return profileDetailJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = profileDetailJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(profileDetailJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return profileDetailJSON.toString() ;
    }
}
