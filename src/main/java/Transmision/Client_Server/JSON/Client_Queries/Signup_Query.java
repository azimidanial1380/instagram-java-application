package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Signup_Query {

    public static JSONObject signupJSON ;

    // for client
    public Signup_Query() {}
    // for server
    public Signup_Query(String JSONAlike) throws JSONException {
        this.signupJSON = new JSONObject(JSONAlike);
    }

    // for client
    public static void setSignupJSONObject(String id , String pass) throws JSONException {
        String signup = "{\"request\":\"signup\"," +
                "\"data\":{" +
                "\"username\":\"" + id + "\"," +
                "\"password\":\"" + pass + "\"" +
                "}" +
                "}";
        signupJSON = new JSONObject(signup);
    }

    // for client
    public static JSONObject getSignupJSONObject() {
        return signupJSON;
    }

    // for server
    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = signupJSON.get(key).toString();
        return value;
    }

    // for server
    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(signupJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return signupJSON.toString() ;
    }
}

