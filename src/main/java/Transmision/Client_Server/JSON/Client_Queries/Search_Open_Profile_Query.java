package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Search_Open_Profile_Query {
    public static JSONObject searchOpenProfileJSON ;


    public Search_Open_Profile_Query() {}

    public Search_Open_Profile_Query(String JSONAlike) throws JSONException {
        this.searchOpenProfileJSON = new JSONObject(JSONAlike);
    }


    public static void setSearchOpenProfileJSONObject(String username ) throws JSONException {
        String searchOpenProfile = "{\"request\":\"searchOpenProfile\"," +
                "\"data\":{" +
                "\"username\":\"" + username + "\"" +
                "}" +
                "}";
        searchOpenProfileJSON = new JSONObject(searchOpenProfile);
    }


    public static JSONObject getSearchOpenProfileJSONObject() {
        return searchOpenProfileJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = searchOpenProfileJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(searchOpenProfileJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return searchOpenProfileJSON.toString() ;
    }

}
