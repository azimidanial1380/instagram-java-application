package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Login_Query {

    public static JSONObject loginJSON ;

    // for client
    public Login_Query() {}
    // for server
    public Login_Query(String JSONAlike) throws JSONException {
        this.loginJSON = new JSONObject(JSONAlike);
    }

    // for client
    public static void setLoginJSONObject(String id , String pass) throws JSONException {
        String signup = "{\"request\":\"login\"," +
                "\"data\":{" +
                "\"username\":\"" + id + "\"," +
                "\"password\":\"" + pass + "\"" +
                     "}" +
                "}";
        loginJSON = new JSONObject(signup);
    }

    // for client
    public static JSONObject getLoginJSONObject() {
        return loginJSON;
    }

    // for server
    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = loginJSON.get(key).toString();
        return value;
    }

    // for server
    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(loginJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return loginJSON.toString() ;
    }
}
