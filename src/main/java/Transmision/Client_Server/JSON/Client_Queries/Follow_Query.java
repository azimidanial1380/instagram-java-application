package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Follow_Query {
    public static JSONObject followJSON ;


    public Follow_Query() {}

    public Follow_Query(String JSONAlike) throws JSONException {
        this.followJSON = new JSONObject(JSONAlike);
    }


    public static void setFollowJSONObject(String follower_Id , String to_Be_Followed_Id ) throws JSONException {
        String follow = "{\"request\":\"follow\"," +
                "\"data\":{" +
                "\"follower_Id\":\"" + follower_Id + "\"," +
                "\"to_Be_Followed_Id\":\"" + to_Be_Followed_Id + "\"" +
                "}" +
                "}";
        followJSON = new JSONObject(follow);
    }


    public static JSONObject getFollowJSONObject() {
        return followJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = followJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(followJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return followJSON.toString() ;
    }
}
