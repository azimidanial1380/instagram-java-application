package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Post_Query {
    public static JSONObject postJSON ;


    public Post_Query() {}

    public Post_Query(String JSONAlike) throws JSONException {
        this.postJSON = new JSONObject(JSONAlike);
    }


    public static void setPostJSONObject(String caption , String time , String fileFormat ) throws JSONException {
        String post = "{\"request\":\"post\"," +
                "\"data\":{" +
                "\"caption\":\"" + caption + "\"," +
                "\"time\":\"" + time + "\"," +
                "\"fileFormat\":\"" + fileFormat + "\"" +
                "}" +
                "}";
        postJSON = new JSONObject(post);
    }


    public static JSONObject getPostJSONObject() {
        return postJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = postJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(postJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return postJSON.toString() ;
    }
}
