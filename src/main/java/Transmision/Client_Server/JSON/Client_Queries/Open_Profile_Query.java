package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Open_Profile_Query {
        public static JSONObject OpenProfileJSON ;


        public Open_Profile_Query() {}

        public Open_Profile_Query(String JSONAlike) throws JSONException {
            this.OpenProfileJSON = new JSONObject(JSONAlike);
        }


        public static void setOpenProfileJSONObject(String username ) throws JSONException {
            String openProfile = "{\"request\":\"openProfile\"," +
                    "\"data\":{" +
                        "\"username\":\"" + username + "\"" +
                        "}" +
                    "}";
            OpenProfileJSON = new JSONObject(openProfile);
        }


        public static JSONObject getOpenMyProfileJSONObject() {
            return OpenProfileJSON;
        }


        public static String generalKeyToValueMap(String key) throws JSONException {
            String value = OpenProfileJSON.get(key).toString();
            return value;
        }


        // for inside Object of the key: "data" to get values of inside JSON object
        public static String dataKeyToValueMap(String key) throws JSONException {
            JSONObject dataObject = new JSONObject(OpenProfileJSON.get("data").toString());
            String value = dataObject.get(key).toString();
            return value;
        }

        @Override
        public String toString() {
            return OpenProfileJSON.toString() ;
        }


}
