package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Comment_Query {
    public static JSONObject commentJSON ;


    public Comment_Query() {}

    public Comment_Query(String JSONAlike) throws JSONException {
        this.commentJSON = new JSONObject(JSONAlike);
    }


    public static void setCommentJSONObject(String text , String time ,String username ,String postId ) throws JSONException {
        String comment = "{\"request\":\"comment\"," +
                "\"data\":{" +
                "\"text\":\"" + text + "\"," +
                "\"time\":\"" + time + "\"," +
                "\"username\":\"" + username + "\"," +
                "\"postId\":\"" + postId + "\"" +
                "}" +
                "}";
        commentJSON = new JSONObject(comment);
    }


    public static JSONObject getCommentJSONObject() {
        return commentJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = commentJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(commentJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return commentJSON.toString() ;
    }
}
