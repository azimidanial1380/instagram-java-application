package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Search_Query {
    public static JSONObject searchJSON ;


    public Search_Query() {}

    public Search_Query(String JSONAlike) throws JSONException {
        this.searchJSON = new JSONObject(JSONAlike);
    }


    public static void setSearchJSONObject(String search_Text ) throws JSONException {
        String search = "{\"request\":\"search\"," +
                "\"data\":{" +
                "\"search_Text\":\"" + search_Text + "\"" +
                "}" +
                "}";
        searchJSON = new JSONObject(search);
    }


    public static JSONObject getSearchJSONObject() {
        return searchJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = searchJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(searchJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return searchJSON.toString() ;
    }
}
