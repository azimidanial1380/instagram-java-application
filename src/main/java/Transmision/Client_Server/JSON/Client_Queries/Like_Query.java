package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Like_Query {
    public static JSONObject likeJSON ;


    public Like_Query() {}

    public Like_Query(String JSONAlike) throws JSONException {
        this.likeJSON = new JSONObject(JSONAlike);
    }


    public static void setLikeJSONObject(String username , String postId ) throws JSONException {
        String like = "{\"request\":\"like\"," +
                "\"data\":{" +
                "\"username\":\"" + username + "\"," +
                "\"postId\":\"" + postId + "\"" +
                "}" +
                "}";
        likeJSON = new JSONObject(like);
    }


    public static JSONObject getLikeJSONObject() {
        return likeJSON;
    }


    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = likeJSON.get(key).toString();
        return value;
    }


    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(likeJSON.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return likeJSON.toString() ;
    }
}
