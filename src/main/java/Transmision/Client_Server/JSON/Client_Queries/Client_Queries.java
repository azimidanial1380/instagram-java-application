package Transmision.Client_Server.JSON.Client_Queries;

import org.json.JSONException;
import org.json.JSONObject;

public class Client_Queries {
    
    private static JSONObject JSONObj ;
    private String jsonRequest ;
    private String jsonData ;
    
    public Client_Queries() throws JSONException {
        String jsonString = "{\"request\":\"" + jsonRequest +"\"," +
                "\"data\":{"  + jsonData  + "}" + "}";
        JSONObj = new JSONObject(jsonString);
    }
    public Client_Queries(String JSONAlike) throws JSONException {
        this.JSONObj = new JSONObject(JSONAlike);
    }
    
    //**// Query functions:

    public void loginJSON(String id ,String pass){
        this.jsonRequest = "login" ;
        this.jsonData = "\"username\":\"" + id + "\"," +
                        "\"password\":\"" + pass + "\"" ;
    }
    public void signupJSON(String id ,String pass){
        this.jsonRequest = "signup" ;
        this.jsonData = "\"username\":\"" + id + "\"," +
                        "\"password\":\"" + pass + "\"" ;
    }
    public void logoutJSON(){
        this.jsonRequest = "logout" ;
        this.jsonData = "\"logout\":\"" + "true" + "\"" ;
    }
    public void postJSON(String postId ,String postName,String postSize,String postCaption){
        this.jsonRequest = "post" ;
        this.jsonData = "\"postId\":\"" + postId + "\"," +
                        "\"postName\":\"" + postName + "\"," +
                        "\"postSize\":\"" + postSize + "\"," +
                        "\"postCaption\":\"" + postCaption + "\"" ;
    }
    public void likeJSON(String postId ,String user){
        this.jsonRequest = "like" ;
        this.jsonData = "\"postId\":\"" + postId + "\"," +
                        "\"user\":\"" + user + "\"" ;
    }
    public void commentJSON(String text, String postId, String user){
        this.jsonRequest = "comment" ;
        this.jsonData = "\"text\":\"" + text + "\"," +
                        "\"postId\":\"" + postId + "\"," +
                        "\"user\":\"" + user + "\"" ;
    }
    public void followJSON(String currentUser, String userToFollow){
        this.jsonRequest = "follow" ;
        this.jsonData = "\"currentUser\":\"" + currentUser + "\"," +
                        "\"userToFollow\":\"" + userToFollow + "\"" ;
    }
    public void unfollowJSON(String currentUser, String userToUnfollow){
        this.jsonRequest = "unfollow" ;
        this.jsonData = "\"currentUser\":\"" + currentUser + "\"," +
                "\"userToUnfollow\":\"" + userToUnfollow + "\"" ;
    }
    public void searchJSON(String searchText){
        this.jsonRequest = "search" ;
        this.jsonData = "\"searchText\":\"" + searchText + "\"" ;
    }

    //*******************************************************************************
    
    // for client
    public static JSONObject getJSONObjObject() {
        return JSONObj;
    }

    // for server
    public static String generalKeyToValueMap(String key) throws JSONException {
        String value = JSONObj.get(key).toString();
        return value;
    }

    // for server
    // for inside Object of the key: "data" to get values of inside JSON object
    public static String dataKeyToValueMap(String key) throws JSONException {
        JSONObject dataObject = new JSONObject(JSONObj.get("data").toString());
        String value = dataObject.get(key).toString();
        return value;
    }

    @Override
    public String toString() {
        return JSONObj.toString() ;
    }
    
}
