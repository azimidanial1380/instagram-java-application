package Back_End.Database;



import Back_End.Server.Post.Comment;
import Back_End.Server.Post.Post;
import Back_End.Server.Profile.Profile;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Insert extends Connection implements Back_End.Database.Interfaces.Insert {

    public Insert() throws SQLException {
    }

    @Override
    public void newUser(String username , String password){
        String userid = UUID.randomUUID().toString();
        super.query = "insert into UserInfo (userid , username , password) values( '%s' , '%s' , '%s' )";
        super.query =String.format(query, userid,username,password);
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        newUserData(userid);

    }
    private void newUserData(String userid){
        super.query = "insert into userprivacy (userid , Follows , Followers , Posts , userProfileImage) values( '%s' , %s , %s, %s , '%s' )";
        super.query =String.format(query,userid,0,0,0,null);
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Boolean isUserSignUped(String userName )  {

        super.query ="select * from UserInfo" ;
        try {
            ResultSet result  = super.state.executeQuery(query);
            while(result.next()){            //check username to be unique
                String databaseUserInfo= result.getNString(2);
                if (databaseUserInfo.equals(userName)){
                    return true ;
                }
            }
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
        return false ;
    }

    @Override
    public Boolean checkUser(String name, String password){
        try
        {
            this.query = "select * from UserInfo";
            ResultSet result = this.state.executeQuery(query);
            while (result.next()){
                String str = result.getNString(2);
                String pass = result.getString(3);
                if (name.equals(str) && pass.equals(password)){
                    return true;
                }
            }
        } catch ( SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }


    @Override
    public String findUserid(String username){
        try {
            this.query = "select * from userinfo";
            ResultSet result = null;
            result = this.state.executeQuery(query);

            while (result.next()){
                String str = result.getNString(2);
                if (str.equals(username)){
                    return result.getNString(1);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace( );
        }
        return null ;
    }

    @Override
    public void addNewPost(Post post){
        super.query = "insert into userpost (userID , username  , postID ,postURL,likeCount,postTime,PostCaption) values( '%s' , '%s' , '%s', '%s', %s, '%s', '%s' )";
        super.query =String.format(query,findUserid(post.getUsername( )),post.getUsername(),post.getPostId()
                ,post.getPhotoPath(),0,post.getTime(),post.getText());
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    @Override
    public Boolean canUserLikePost(String postID , String userLikePost){
        try
        {
            this.query = "select * from postlikes";
            ResultSet result = this.state.executeQuery(query);
            while (result.next()){
                String str = result.getNString(1);
                String pass = result.getString(2);
                if (postID.equals(str) && userLikePost.equals(pass)){
                    return false;
                }
            }
        } catch ( SQLException throwables) {
            throwables.printStackTrace();
        }
        return true;
    }


    @Override
    public void addNewCommentToPost(String postID , String username , String comment ,String commentTime){
        super.query = "insert into postcomments (postID , comment , usernameOfComment , commentTime ) values( '%s' , '%s' , '%s', '%s' )";
        super.query =String.format(query,postID,username,comment, LocalDateTime.now()    );
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }


    @Override
    public ArrayList<Post> getPosts(String username){
        ArrayList<Post> allPosts = new ArrayList<>();
         try
        {
            this.query = "select * from userpost";
            ResultSet result = this.state.executeQuery(query);
            while (result.next()){
                String str = result.getNString(2);
                if (str.equals(username)){
                    setPostData(username, allPosts, result);
                }
            }
            for (int i = 0; i < allPosts.size( ); i++) {
                allPosts.get(i).setComments(getComments(allPosts.get(i).getPostId()));
            }
        } catch ( SQLException throwables) {
            throwables.printStackTrace();
        }

        return allPosts;
    }

    private void setPostData(String username, ArrayList<Post> allPosts, ResultSet result) throws SQLException{
        Post post = new Post(username, result.getNString(7));
        post.setPostId(result.getNString(3));
        post.setPhotoPath(result.getNString(4));
        post.setTime(result.getTimestamp(6).toString());
        post.setLikes(result.getInt(5));
//        post.setComments(getComments(result.getNString(3)));
        allPosts.add(post);
    }

    public ArrayList<String> search(String searchText) throws SQLException{
        ArrayList<String> result = new ArrayList<>();
        query = "select * from userinfo ";
        ResultSet resultSet = state.executeQuery(query);
        while (resultSet.next()){
            String str = resultSet.getNString(2);
             if ( str.equals(searchText) ){
                 result.add(str);
             }
        }
        return result ;
    }

    public void addUsernameLike(String postID ,String username) throws SQLException{
        super.query = "insert into postlikes (postid , usernameOfLike) values( '%s' , '%s'  )";
        super.query =String.format(query, postID,username);
        state.execute(query);
    }

    public void follow (String followerUsername , String followingUsername) throws SQLException{
        super.query = "insert into follow (followerUserID , followerUsername , followingUserID, followingUsername)" +
                " values( '%s' , '%s', '%s' , '%s'  )";
        super.query =String.format(query, findUserid(followerUsername), followerUsername
                , findUserid(followingUsername) , followingUsername);
        state.execute(query);
        Update update = new Update();
        update.plusUserFollowing(findUserid(followerUsername));
        update.plusUserFollowers(findUserid(followingUsername));
    }

    public ArrayList<String> findFollowers(String username) throws SQLException{
        ArrayList<String> followers= new ArrayList<>();
         this.query = "select * from follow";
        ResultSet result = this.state.executeQuery(query);
        while (result.next()){
            String follower1 = result.getNString(2);
            String following = result.getNString(4);
            if (follower1.equals(username) ){
                followers.add(following);
            }
        }
        followers.add(username);
        return followers ;
    }

    public Profile findProfile(String userName) throws SQLException{
        Profile profile = new Profile();
        String userID = findUserid(userName);
        this.query = "select * from userprivacy";
        ResultSet result = this.state.executeQuery(query);
        while (result.next()){
            String str = result.getNString(1);
            if (str.equals(userID)){
                profile.setPostNumber(String.valueOf(result.getInt(4 )));
                profile.setFollowerNumber(String.valueOf(result.getInt( 3)));
                profile.setFollowingNumber(String.valueOf(result.getInt(2 )));
            }
        }
        return profile ;
    }

    public String isUserFollowerOrFollowing(String follower ,String toBeFollowed) throws SQLException{
        this.query = "select * from follow";
        ResultSet result = this.state.executeQuery(query);
        while (result.next()){
            String follower1 = result.getNString(2);
            String following = result.getNString(4);
            if ( follower1.equals(follower) && following.equals(toBeFollowed) ){
                return "following" ;
            }if ( following.equals(follower) && follower1.equals(toBeFollowed) ){
                return "follower" ;
            }
        }
        return "editProfile" ;
    }


    public Boolean canUserBeFollower(String follower,String toBeFollowed) throws SQLException{
        if ( isUserFollowerOrFollowing(follower,toBeFollowed).equals("following") ){
            return false;
        }else {
            return true;
        }
    }











    public void addComment(Comment comment){
        super.query = "insert into postcomments (postID , comment , usernameOfComment , commentTime)" +
                " values( '%s' , '%s' , '%s' , '%s' )";
        super.query =String.format(query,comment.getPostId() , comment.getText(),
                comment.getUsername() , comment.getTime());
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Comment> getComments(String postID) throws SQLException{

        List<Comment> comments = new ArrayList<>(  );

        this.query = "select * from postcomments";

        ResultSet result = this.state.executeQuery(query);
        while (result.next()){
            String str = result.getNString(1);
            if ( str.equals(postID) ){
                Comment comment = new Comment(result.getNString( 2 ),result.getNString( 3 ) );
                comment.setTime(String.valueOf(result.getTimestamp(4)));
                comments.add(comment);
            }

        }
        return comments;
    }














































}
