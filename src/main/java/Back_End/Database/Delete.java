package Back_End.Database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

public class Delete extends Connection {
    public Delete() throws SQLException{
    }

    public void unfollow(String followerUsername , String followingUsername) throws SQLException{
        this.query = "DELETE FROM  follow where followerUsername = '%s'";
        super.query =String.format(query, followerUsername);
        state.execute(query);
        Insert insert = new Insert();
        Update update = new Update();
        update.deductUserFollowing(insert.findUserid(followerUsername));
        update.deductUserFollowers(insert.findUserid(followingUsername));
    }

    public void removeUsernameLike(String postID) throws SQLException{
        this.query = "DELETE FROM postlikes where postid = '%s'";
        super.query =String.format(query, postID);
        state.execute(query);
    }

}
