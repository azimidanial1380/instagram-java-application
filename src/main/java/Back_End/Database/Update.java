package Back_End.Database;


import java.sql.ResultSet;
import java.sql.SQLException;

public class Update extends Connection {
    public Update() throws SQLException{
    }

    public int findPostLikeNumber(String postID) throws SQLException{
          this.query = "select * from userpost";
          ResultSet result = null;
          result = this.state.executeQuery(query);
          while (result.next()){
              String str = result.getNString(3);
              if (str.equals(postID)){
                  return result.getInt(5);
              }
          }
          return 0 ;
    }

    public void likePost(String postID, String userLikePost) throws SQLException{
        Insert insert = new Insert();
        int like = findPostLikeNumber(postID);
        if (! insert.canUserLikePost(postID,userLikePost) ){
            --like;
            Delete delete = new Delete();
            delete.removeUsernameLike(postID);
        }else {
            ++like;
            insert.addUsernameLike(postID,userLikePost);
        }
        this.query = "update userpost set likeCount=%s where postid='%s'";
        this.query = String.format(query,like,postID);
        this.state.execute(query);
    }

    public void follow(String username , String followUsername){

    }

    public int findFollowersNumber(String userID) throws SQLException{
        this.query = "select * from userprivacy";
        ResultSet result = null;
        result = this.state.executeQuery(query);
        while (result.next()){
            String str = result.getNString(1);
            if (str.equals(userID)){
                return result.getInt(3);
            }
        }
        return 0 ;
    }
    public int findFollowingNumber(String userID) throws SQLException{
        this.query = "select * from userprivacy";
        ResultSet result = null;
        result = this.state.executeQuery(query);
        while (result.next()){
            String str = result.getNString(1);
            if (str.equals(userID)){
                return result.getInt(2);
            }
        }
        return 0 ;
    }

    public void plusUserFollowing(String userID) throws SQLException{
        int followersCount = findFollowingNumber(userID);
        followersCount+=1;
        System.out.println(followersCount + "//////////////");
        this.query = "UPDATE userprivacy set Follows=%s where userid='%s'";
        this.query = String.format(query,followersCount,userID);
        this.state.execute(query);
    }

    public void plusUserFollowers(String userID) throws SQLException{
        int followersCount = findFollowersNumber(userID);
        followersCount+=1;
        this.query = "UPDATE userprivacy set Followers=%s where userid='%s'";
        this.query = String.format(query,followersCount,userID);
        this.state.execute(query);
    }

    public void deductUserFollowing(String UserID) throws SQLException{
        int followersCount = findFollowingNumber(UserID);
        followersCount+=-1;
        this.query = "UPDATE userprivacy set Follows=%s where userid='%s'";
        this.query = String.format(query,followersCount,UserID);
        this.state.execute(query);
    }

    public void deductUserFollowers(String userID) throws SQLException{
        int followersCount = findFollowersNumber(userID);
        followersCount+=-1;
        this.query = "UPDATE userprivacy set Followers=%s where userid='%s'";
        this.query = String.format(query,followersCount,userID);
        this.state.execute(query);
    }




}
