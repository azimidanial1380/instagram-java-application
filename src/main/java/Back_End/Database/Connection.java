package Back_End.Database;


import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Connection {

    private java.sql.Connection connect = null;
    private String url = null;
    protected Statement state = null;
    protected String query = null ;

    public Connection () throws SQLException {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            url = "jdbc:mysql://localhost:3306/Instagram?user=root";
            connect = DriverManager.getConnection(url);
            state = connect.createStatement();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }

    }

    protected void closeUp(){
        try {
            state.close();
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }





}

