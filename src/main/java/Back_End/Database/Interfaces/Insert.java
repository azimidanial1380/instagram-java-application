package Back_End.Database.Interfaces;

import Back_End.Server.Post.Post;

import java.util.ArrayList;

public interface Insert {

    ArrayList<Post> getPosts(String username);
    void addNewCommentToPost(String postID, String username, String comment, String commentTime);
    Boolean canUserLikePost(String postID, String userLikePost);
    Boolean checkUser(String name, String password);
    String findUserid(String username);
    void addNewPost(Post post);
    Boolean isUserSignUped(String userName) ;
    void newUser(String username, String password);


}

