package Back_End.Server;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileServer {


    public static void receiveFile(DataOutputStream dos, DataInputStream dis, String filePath) throws IOException {
        long readData = 0;

        int transferSize = 1048576;
        long fileSize = dis.readLong();
        byte[] buffer = new byte[transferSize];

        File file = new File(filePath);
        file.getParentFile().mkdirs(); // correct!
        if (!file.exists()) {
            file.createNewFile();
        }

        FileOutputStream fos = new FileOutputStream(file, true);

        int len;

        while (true) {
            len = dis.read(buffer);
            readData = readData + (long) len;

            byte[] tmp = new byte[len];
            System.arraycopy(buffer, 0, tmp, 0, len);
            fos.write(tmp);
            fos.flush();
            if (readData == fileSize) {
                break;
            }
        }

        dos.writeUTF("finished");
        fos.close();
    }

    public static void sendFile(DataInputStream dis, DataOutputStream dos, String filePath) throws IOException {
        int transferSize = 1048576;

        File file = new File(filePath);
        Path path = Paths.get(filePath);

        dos.writeLong(Files.size(path));
        dos.flush();

        FileInputStream fis = new FileInputStream(file);

        byte[] buffer = new byte[transferSize];

        int len = fis.available();
        System.out.println(len );
        while (len > 0) {
            len = fis.read(buffer);
            if (len < transferSize) {
                byte[] tmp = new byte[len];
                System.arraycopy(buffer, 0, tmp, 0, len);
                dos.write(tmp);
                dos.flush();
                break;
            }
            dos.write(buffer);
            dos.flush();
            if (fis.available() == 0) {
                break;
            }
        }
        System.out.println("hiiiii" );
        dis.readUTF();
        fis.close();
    }
}
