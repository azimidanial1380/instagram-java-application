package Back_End.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.*;

public class    MainServer extends Thread {
    private static final int port = 22222;
    private static MainServer singleServer = null;
    private ServerSocket ss;
    private static Set<Handler> handlers;

    private MainServer() {
        this.handlers = new HashSet<>();

        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static MainServer getSingleServer () {

        if (singleServer == null) {
            singleServer = new MainServer();
        }
        return singleServer;
    }

//    public Set<Handler> getHandlers() {
//        Set<Handler> output = new HashSet<E>(handlers);
//        return output;
//
//    }

    @Override
    public void run() {
        Handler newHandler = null;
        while (true) {
            try {
                newHandler = new Handler(ss.accept());
                newHandler.start();
                handlers.add(newHandler);



            } catch (IOException | SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
