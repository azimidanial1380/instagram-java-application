package Back_End.Server.Post;

import java.util.List;


public class Post {

    private String postId;
    private String username;
    private String photoPath;
    private String text;
    private String time;
    private int likes;
    private List<Comment> comments;

    public Post(String username, String text) {
        this.username = username;
        this.text = text;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public void setPhotoPath(String path) {
        this.photoPath =  path;
    }

    public void setLikes (int likes) {
        this.likes = likes ;
    }

    public void setText(String text){
        this.text = text;
    }

    public int getLikes() {
        return likes;
    }

    public String getPostId() {
        return postId;
    }

    public String getUsername() {
        return username;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public String getText() {
        return text;
    }

    public String getTime() {
        return time;
    }

}
