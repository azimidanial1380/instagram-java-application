package Back_End.Server.Post;

import java.util.List;

public class User {
    private String username;
    private String password;
    private String profilePhotoName;// it can be the password
    private List<Post> posts;
    private List<String> followersID;
    private List<String> followingsID;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public void setProfilePhotoName(String profilePhotoName, String format) {
        this.profilePhotoName = profilePhotoName + "." + format;
    }

    public void addPost (Post post) {
        this.posts.add(post);
    }

    public void addFollower (String ID) {
        this.followersID.add(ID);
    }

    public void addFollowing (String ID) {
        this.followingsID.add(ID);
    }
}
