package Back_End.Server.Post;

public class Comment {

    private String text;
    private String username;
    private String time;
    private String postId;

    public String getPostId(){
        return postId;
    }

    public void setPostId(String postId){
        this.postId = postId;
    }

    public String getTime(){
        return time;
    }

    public void setTime(String time){
        this.time = time;
    }

    public Comment(String text, String username) {
        this.text = text ;
        this.username = username ;
    }

    public String getText() {
        return text;
    }

    public String getUsername() {
        return username;
    }
}
