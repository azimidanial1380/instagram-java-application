package Back_End.Server;

import Back_End.Database.Delete;
import Back_End.Database.Insert;
import Back_End.Database.Update;
import Back_End.Server.Post.Comment;
import Back_End.Server.Post.Post;
import Back_End.Server.Profile.Profile;
import Transmision.Client_Server.JSON.Client_Queries.*;
import Transmision.Client_Server.JSON.Server_Queries.*;
import org.json.JSONException;

import java.io.*;
import java.net.Socket;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Handler extends Thread{

    private Socket socket;
    private DataInputStream dis;
    private DataOutputStream dos;
    private String password;
    private String username;
    private List<Notification> notifications;

    public Handler(Socket socket) throws SQLException{
        this.socket = socket;
        try {
            this.dis = new DataInputStream(socket.getInputStream());
            this.dos = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public synchronized void addNotification (Notification notification) {
        this.notifications.add(notification);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }



    @Override
    public void run() {
        startupMessage();
        try {
            messageProcess();
        } catch (IOException | JSONException | SQLException e) {
            e.printStackTrace();
        }
        // }

    }

    /** Sends direct messages.*/
    public void directMessagesCheck () {

    }

    /** Sends notifications.*/
    public void notificationsCheck () {

    }

    /**
     * The main method to process the received message.
     * Processes the received message from client, uses the database,
     * and sends the appropriate message as
     * the answer.
     * */


    public void messageProcess () throws IOException, JSONException, SQLException {

        String check = dis.readUTF();
        System.out.println(check + " -------------->Check" );
        switch (check){
            case "Comment":
                addComment();
            case "addPost":
                addPost();
            case "getPost":
                sendUserPosts();
            case "like":
                addLike();
                break;
            case "follow":
                addFollow();
                break;
            case "search":
                search();
                break;
            case "Profile":
                Profile();
                break;
            case "signup":
                signupProcess();
                break;
            default:
                loginProcess();
                break;
        }
    }

    public void addFollow() throws IOException, SQLException {
        Follow_Query follow = new Follow_Query(dis.readUTF( ));
        String follower = follow.dataKeyToValueMap("follower_Id") ;
        String toBeFollowed  = follow.dataKeyToValueMap("to_Be_Followed_Id") ;
        Insert insert = new Insert();
        System.out.println( insert.canUserBeFollower(follower,toBeFollowed)  );
        if ( insert.canUserBeFollower(follower,toBeFollowed) ){
            insert.follow(follower,toBeFollowed);
            }else {
            Delete delete = new Delete();
            delete.unfollow(follower,toBeFollowed);
            }
        dos.writeUTF("proccess is done");
            messageProcess();

    }

    public void Profile() throws IOException, SQLException{
        Open_Profile_Query profile = new Open_Profile_Query(dis.readUTF());
        String username = profile.dataKeyToValueMap("username");

        Insert insert = new Insert();
        Profile profile1 = insert.findProfile(username);
        profile1.setUserType(insert.isUserFollowerOrFollowing(getUsername(),username));

        sendProfile(profile1,username);

        dos.writeUTF("Profile Sent.");
        messageProcess();
    }
    public void sendProfile(Profile profile , String username) throws IOException {
        Profile_Detail_Query profileDetail = new Profile_Detail_Query();
        profileDetail.setProfileDetailJSONObject( username , profile.getUserType()
                ,profile.getPostNumber(),profile.getFollowerNumber(),profile.getFollowingNumber());
        System.out.println( profile.getFollowingNumber() +" ----->");
        dos.writeUTF(profileDetail.toString());
        dos.flush();
    }

    public void sendUserPosts() throws SQLException, IOException{
        String username = dis.readUTF( );
        Insert insert = new Insert();

        ArrayList<String> followers = insert.findFollowers(username);
        dos.write(followers.size());

        for (int i = 0; i < followers.size( ); i++) {

            ArrayList<Post> allPosts = insert.getPosts(followers.get(i));

            dos.write(allPosts.size());
            dos.flush();
            System.out.println(allPosts.size());
            for (Post allPost : allPosts) {
                sendPost(allPost);
            }
        }
        System.out.println("finished" );
        messageProcess();
    }

    public void sendPost(Post post) throws IOException {
        Post_List sendPost = new Post_List() ;
        sendPost.setPostListJSONObject(post.getText() , post.getTime()
                ,String.valueOf(post.getLikes()),post.getUsername(),
                post.getPostId(), getFormat(post.getPhotoPath()));
        dos.writeUTF(sendPost.toString());

        dos.flush();
        sendComments(post);
        FileServer.sendFile(dis,dos , post.getPhotoPath());
        System.out.println(dis.readUTF());

    }

    private String getFormat(String dir){
        String [] fileFormat = dir.split("\\.") ;
        return fileFormat[1] ;
    }
    public void addPost() throws IOException, SQLException{

        Post_Query posting = new Post_Query(dis.readUTF( ));

        String caption = posting.dataKeyToValueMap("caption");
        String time = posting.dataKeyToValueMap("time");
        String fileFormat = posting.dataKeyToValueMap("fileFormat");
        String postID = UUID.randomUUID().toString() ;
        String filePath = ("Photos" + "\\\\" + postID+ "." + fileFormat) ;

        FileServer.receiveFile(dos,dis , filePath );

        Post newPost = new Post(this.username , caption);
        newPost.setPostId(postID);
        newPost.setPhotoPath(filePath);
        newPost.setTime(time);
        newPost.setText(caption);

        Insert insert = new Insert();
        insert.addNewPost(newPost);

        dos.writeUTF("adding post id finished");

        messageProcess();
    }

    public void addLike() throws IOException, SQLException{
        Like_Query like = new Like_Query(dis.readUTF( ));
        String username = like.dataKeyToValueMap("username") ;
        String postid  = like.dataKeyToValueMap("postId") ;

        Update update = new Update();
        update.likePost(postid,username);
        dos.writeUTF("proccess is done");
        messageProcess();

    }
    public void search() throws IOException, SQLException{
        Search_Query search = new Search_Query(dis.readUTF( ));

        Insert insert = new Insert();
        ArrayList<String> searchResult = insert.search(search.dataKeyToValueMap("search_Text"));
        sendSearchResults(searchResult);
    }
    public void sendSearchResults(ArrayList<String> searchResult) throws SQLException, IOException{

        dos.writeUTF(String.valueOf(searchResult.size()));
        dos.flush();
        for (String foundUser : searchResult) {

            sendSearchResult(foundUser);

        }
        System.out.println("done" );
        messageProcess();

    }
    public void sendSearchResult(String foundUser) throws IOException {
        Search_Response_Query searchResponse = new Search_Response_Query() ;
        searchResponse.setSearchResponseJSONObject(foundUser);
        dos.writeUTF(searchResponse.toString());
        dos.flush();

    }


    public void loginProcess() throws JSONException, IOException, SQLException{
            System.out.println("login" );
                Login_Query login = new Login_Query(dis.readUTF( ));

                System.out.print("username:  ");
                System.out.println(login.dataKeyToValueMap("username"));
                System.out.print("password:  ");
                System.out.println(login.dataKeyToValueMap("password"));

                Insert ins = new Insert( );
                boolean isLoginValid = ins.checkUser(login.dataKeyToValueMap("username"), login.dataKeyToValueMap("password"));
                dos.writeUTF("User and Password received in Server!");

                Login_Response_Query response = new Login_Response_Query();


                if ( isLoginValid ) {
                    response.setResponseJSONObject("ValidateLogin");
                    dos.writeUTF(response.toString());
                    this.username = login.dataKeyToValueMap("username");
                    this.password = login.dataKeyToValueMap("password");
                } else {
                    response.setResponseJSONObject("InvalidateLogin");
                    dos.writeUTF(response.toString( ));
        }
        messageProcess();
    }

    public void signupProcess() throws JSONException, IOException, SQLException{
            Signup_Query signup = new Signup_Query(dis.readUTF( ));

            System.out.print("data (JSON String) : ");
            System.out.println(signup.generalKeyToValueMap("data"));

            System.out.print("username:  ");
            System.out.println(signup.dataKeyToValueMap("username"));
            System.out.print("password:  ");
            System.out.println(signup.dataKeyToValueMap("password"));
            Insert ins = new Insert( );
            Signup_Response_Query signupResponse = new Signup_Response_Query( );
            boolean isSignupValid = ins.isUserSignUped(signup.dataKeyToValueMap("username"));

            if ( ins.isUserSignUped(signup.dataKeyToValueMap("username")) ) {
                signupResponse.setResponseJSONObject("InvalidateSignup");
                dos.writeUTF(signupResponse.toString( ));
                System.out.println(signupResponse);
            } else {
                ins.newUser(signup.dataKeyToValueMap("username"), signup.dataKeyToValueMap("password"));
                signupResponse.setResponseJSONObject("ValidateSignup");
                dos.writeUTF(signupResponse.toString( ));
                this.username = signup.dataKeyToValueMap("username");
                this.password = signup.dataKeyToValueMap("password");
            }
        messageProcess();
    }



    public void addComment() throws IOException, SQLException{

        Comment_Query comment = new Comment_Query(dis.readUTF( ));

        Comment comment1 = new Comment(comment.dataKeyToValueMap("text"),comment.dataKeyToValueMap("username"));
        comment1.setTime(comment.dataKeyToValueMap("time"));
        comment1.setPostId(comment.dataKeyToValueMap("postId") );

        Insert insert = new Insert();
        insert.addComment(comment1);

        dos.writeUTF("Comment is added.");

        messageProcess();

    }
    public void sendComments(Post post) throws IOException {
        dos.writeUTF(String.valueOf(post.getComments().size()));
        dos.flush();
        for (int i = 0 ; i <post.getComments().size() ; i++ ) {
            sendComment( post.getComments().get(i) );
        }

    }
    public void sendComment(Comment comment) throws IOException {
        Comment_List_Query commentList = new Comment_List_Query();
        commentList.setCommentListJSONObject( comment.getText(),comment.getUsername() );
        dos.writeUTF(commentList.toString());
        dos.flush();

    }




























    /** Sends the first message to the client. The app start page will be loaded.*/
    public void startupMessage () {

    }


    /** Closes the connection.*/
    public void close () {
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
