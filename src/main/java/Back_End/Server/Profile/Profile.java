package Back_End.Server.Profile;

public class Profile {

    private String userType ;
    private String postNumber  ;
    private String followerNumber ;
    private String followingNumber  ;

    public String getUserType(){
        return userType;
    }

    public void setUserType(String userType){
        this.userType = userType;
    }

    public String getPostNumber(){
        return postNumber;
    }

    public void setPostNumber(String postNumber){
        this.postNumber = postNumber;
    }

    public String getFollowerNumber(){
        return followerNumber;
    }

    public void setFollowerNumber(String followerNumber){
        this.followerNumber = followerNumber;
    }

    public String getFollowingNumber(){
        return followingNumber;
    }

    public void setFollowingNumber(String followingNumber){
        this.followingNumber = followingNumber;
    }

}
